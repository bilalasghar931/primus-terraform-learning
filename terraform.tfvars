cidr_blocks=[
    {cidr_block = "10.0.0.0/16", name = "dev-vpc"},
    {cidr_block = "10.0.40.0/24", name = "dev-subnet"}
]
availability_zone="us-east-1b"